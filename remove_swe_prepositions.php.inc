<?php
/**
 * Generate a Readability IndeX for swedish text as per
 * http://www.lix.se
 * 
 * Remove swedish prepositions from text string
 * @param string $s Contains the text to remove prepositions from.
 * @return array $return_arr
 *  Contains three (3) indexes: 'all' contains all words sorted by
 *  occurencie, top5 contains five most occured and 'top10' is the
 *  top ten most frequent words. All strings are in lower format.
 */
function remove_swe_prepositions($text = "", $no_tags = TRUE) {
  if ($text == "") {
    return "";
  }

  // Prepare the text.
  $s = $text;
  if ($no_tags) {
    $s = trim(strip_tags($text));
  }

  // Init vars for LIX:
  // http://lix.se
  // https://sv.wikipedia.org/wiki/L%C3%A4sbarhetsindex

  // LIX-tal för texter av olika slag (Wikipedia):
  //    - 25 Barnböcker.
  // 25 - 30 Enkla texter.
  // 30 - 40 Normaltext / skönlitteratur.
  // 40 - 50 Sakinformation, till exempel Wikipedia.
  // 50 - 60 Facktexter.
  // 60 -    Svåra facktexter / forskning / avhandlingar.

  // http://www.lix.se
  //    < 30 Mycket lättläst, barnböcker
  // 30 - 40 Lättläst, skönlitteratur, populärtidningar
  // 40 - 50 Medelsvår, normal tidningstext
  // 50 - 60 Svår, normalt värde för officiella texter
  // 60 -    Mycket svår, byråkratsvenska
  $word_count = 0;
  $sentence_count = 0;
  $long_words = 0;
  $word_count_prep = 0;
  $long_words_prep = 0;

  $prepositions = array(
    "à",
    "af",
    "à la",
    "allt efter",
    "alltefter",
    "allt ifrån",
    "alltifrån",
    "an",
    "angående",
    "apropå",
    "av",
    "bak",
    "bakanför",
    "bakför",
    "bakom",
    "beroende på",
    "bland",
    "bortanför",
    "bortom",
    "bredvid",
    "efter",
    "emellan",
    "enligt",
    "exklusive",
    "framanför",
    "framför",
    "framom",
    "från",
    "för",
    "före",
    "förutom",
    "förutsatt",
    "genom",
    "gentemot",
    "givet",
    "gånger",
    "hinsides",
    "hitom",
    "hos",
    "i",
    "ifrån",
    "igenom",
    "ikring",
    "i motsats till",
    "inifrån",
    "inklusive",
    "innan",
    "innanför",
    "inom",
    "inpå",
    "intill",
    "inunder",
    "inuti",
    "invid",
    "inåt",
    "i och med",
    "jämlikt",
    "jämte",
    "kontra",
    "kring",
    "långsåt",
    "längs",
    "längs efter",
    "längsefter",
    "längs med",
    "längsmed",
    "med",
    "medels",
    "medelst",
    "med hjälp av",
    "med hänsyn till",
    "med tanke på",
    "mellan",
    "mitt emellan",
    "mittemellan",
    "mittemot",
    "mittimellan",
    "mot",
    "mä",
    "nedanför",
    "nedför",
    "nerför",
    "oansett",
    "oavsett",
    "ofvan",
    "om",
    "ovan",
    "ovanför",
    "ovanpå",
    "per",
    "på",
    "på grund av",
    "re",
    "runt",
    "runtikring",
    "runtom",
    "runtomkring",
    "rörande",
    "sedan",
    "senza",
    "sett till",
    "som",
    "såsom",
    "tack vare",
    "te",
    "till",
    "tillika",
    "tills",
    "till skillnad från",
    "till skillnad mot",
    "trots",
    "tvärt emot",
    "tvärtemot",
    "under",
    "undör",
    "uppefter",
    "uppför",
    "uppå",
    "ur",
    "utan",
    "utanför",
    "utanpå",
    "utav",
    "utefter",
    "utför",
    "uti",
    "utifrån",
    "utmed",
    "utom",
    "utur",
    "utve",
    "utåt",
    "utöver",
    "via",
    "vid",
    "visavi",
    "å",
    "åt",
    "åv",
    "än",
    "ätter",
    "öfver",
    "öfwer",
    "över"
  );

  // Remove sentence ending signs.
  $r_s = strtolower($s);
  $r_s = preg_replace("/(\.|\!|\?|\-)*/", "", $r_s);
  /*
   * See http://forums.phpfreaks.com/topic/166765-solved-preg-replace-to-remove-whole-words-in-string/
   * Mark Baker
   */
  $word_list = str_word_count($r_s, 2, "åäöÅÄÖ");
  $word_list_prep = $word_list;
  foreach ($word_list as $word_list_key => $word_list_word) {
    if (in_array($word_list_word, $prepositions)) {
      unset($word_list[$word_list_key]);
    }
  }
  // End remove sentence ending signs.

  // Word frequency count.
  $sorted_array = array_count_values($word_list);
  arsort($sorted_array);
  $word_count = array_sum($sorted_array);
  $word_count_prep = count($word_list_prep);
  foreach ($word_list_prep as $no => $word) {
    if (strlen($word) > 6) {
      $long_words_prep++;
    }
  }

  // remove where key length = 1
  $sa = array();
  foreach ($sorted_array as $k => $v) {
    if (strlen($k) == 1) {
      unset($sorted_array[$k]);
    } else {
      $sa[$k]['long_word'] = FALSE;
      $sa[$k]['count'] = $v;
      if (strlen($k) > 6) {
        $long_words++;
        $sa[$k]['long_word'] = TRUE;
      }
    }
  }
  $sorted_array = $sa;
  $return_arr['all'] = $sorted_array;

  // Top 5 and top 10.
  $i = 10;
  $item = 'top5';
  foreach ($sorted_array as $k => $v) {
    $return_arr[$item][$k] = $v;
    $i--;
    if ($i == 5) {
      $item = 'top10';
      $return_arr[$item] = $return_arr['top5'];
    }
    if ($i == 0) {
      break;
    }
  }

  // Sentence count.
  $sentences_arr = preg_split("/(\.|\!|\?)/", $s);
  // Remove "empty" lines.
  foreach ($sentences_arr as $no => $sent) {
    if (strlen(trim($sent)) < 1) {
      unset($sentences_arr[$no]);
    } else {
      $sentences_arr[$no] = trim($sent);
    }
  }
  $sentence_count = count($sentences_arr);

  $return_arr["Words"] = $word_count;
  $return_arr["Words with prep"] = $word_count_prep;
  $return_arr["Long words"] = $long_words;
  $return_arr["Long words with prep"] = $long_words_prep;
  $return_arr["Sentences"] = $sentence_count;

  // Läsbarhetsindex: LIX.
  $lix = ($word_count / $sentence_count) + ($long_words * 100 / $word_count);
  $return_arr["LIX no prep"]["lix.float"] = $lix;
  $return_arr["LIX no prep"]["lix"] = (int) $lix;

  // Läsbarhetsindex: LIX with prepositions.
  $lix_prep = ($word_count_prep / $sentence_count) + ($long_words_prep * 100 / $word_count_prep);
  $return_arr["LIX prep"]["lix.float"] = $lix_prep;
  $return_arr["LIX prep"]["lix"] = (int) $lix_prep;

  return $return_arr;
}
